package com.leolei.tradehelmtest;

import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.otto.Subscribe;

import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by leolei on 2016/2/17.
 */

public class MainActivityPresenter extends Presenter {
    private static final java.lang.String CURRENT_RESULTS = "currentResults";
    private WeakReference<IView> viewReference;
    private List<Result> currentResults;

    public MainActivityPresenter(IView view) {
        viewReference = new WeakReference<>(view);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            Type type = new TypeToken<List<Result>>() {
            }.getType();
            currentResults = new Gson().fromJson(savedInstanceState.getString(CURRENT_RESULTS), type);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        EventBus.getInstance().register(this);

        IView view = viewReference.get();
        if (view == null) {
            return;
        }

        setResultsOnView();
    }

    private void setResultsOnView() {
        IView view = viewReference.get();
        if (view == null) {
            return;
        }
        if (currentResults != null && currentResults.isEmpty()) {
            view.setErrorMessage("No se encontraron items");
            return;
        }
        view.setResults(currentResults);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(CURRENT_RESULTS, new Gson().toJson(currentResults));
    }

    @Override
    public void onPause() {
        super.onResume();
        EventBus.getInstance().unregister(this);
    }

    public void onSearchButtonClicked(String q) {
        IView view = viewReference.get();
        if (view == null) {
            return;
        }

        view.showProgress();
        MercadoLibreRepository.getInstance().performSearch(q);
    }

    @Subscribe
    public void onQuerySuccess(MLResult results) {
        currentResults = results.getResults();

        IView view = viewReference.get();
        if (view == null) {
            return;
        }

        view.hideProgress();
        setResultsOnView();
    }

    @Subscribe
    public void onQueryFailure(Throwable t) {
        IView view = viewReference.get();
        if (view == null) {
            return;
        }

        view.hideProgress();
        view.setErrorMessage(t.getMessage());
    }

    public interface IView {

        void showProgress();

        void hideProgress();

        void setResults(List<Result> results);

        void setErrorMessage(String message);
    }
}
