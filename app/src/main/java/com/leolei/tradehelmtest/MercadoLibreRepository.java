package com.leolei.tradehelmtest;

import java.io.IOException;

import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;

import android.os.Handler;
import android.os.Looper;

import com.google.gson.Gson;

/**
 * Created by leolei on 2016/2/17.
 */

public class MercadoLibreRepository {

    private static MercadoLibreRepository instance;
    private OkHttpClient client;
    private Gson gson;

    private MercadoLibreRepository() {
        client = new OkHttpClient();
        gson = new Gson();
    }

    public static MercadoLibreRepository getInstance() {
        if (instance == null) {
            instance = new MercadoLibreRepository();
        }
        return instance;
    }

    private Callback okHttpCallback = new Callback() {
        @Override
        public void onFailure(okhttp3.Call call, final IOException e) {
            e.printStackTrace();
            handler.post(new Runnable() {

                @Override
                public void run() {
                    EventBus.getInstance().post(e);
                }
            });
        }

        private Handler handler = new Handler(Looper.getMainLooper());

        @Override
        public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
            String resultString = response.body().string();
            final MLResult result = gson.fromJson(resultString, MLResult.class);
            handler.post(new Runnable() {

                @Override
                public void run() {
                    EventBus.getInstance().post(result);
                }
            });
        }
    };

    public void performSearch(String q) {
        Request request = new Request.Builder().url("https://api.mercadolibre.com/sites/MLA/search?sort=price_asc&q=" + q).build();
        client.newCall(request).enqueue(okHttpCallback);
    }

}
