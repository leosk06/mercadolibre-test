package com.leolei.tradehelmtest;

/**
 * Created by leolei on 2016/2/17.
 */

public class Result {
    private String title;
    private String subtitle;
    private float price;
    private String currency_id;

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public float getPrice() {
        return price;
    }

    public String getCurrency_id() {
        return currency_id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setCurrency_id(String currency_id) {
        this.currency_id = currency_id;
    }
}
