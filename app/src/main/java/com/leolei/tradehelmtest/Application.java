package com.leolei.tradehelmtest;

/**
 * Created by leolei on 2016/2/17.
 */

public class Application extends android.app.Application {
    private static Application instance;

    public static Application getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
