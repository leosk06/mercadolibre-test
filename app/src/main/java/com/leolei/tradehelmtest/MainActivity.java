package com.leolei.tradehelmtest;

import java.util.List;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements MainActivityPresenter.IView {

    private EditText queryField;
    private Button queryButton;
    private RecyclerView resultList;
    private MainActivityPresenter presenter;
    private MLResultListAdapter adapter;
    private TextView errorMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        queryField = (EditText) findViewById(R.id.field_query);
        queryButton = (Button) findViewById(R.id.button_search);
        resultList = (RecyclerView) findViewById(R.id.list_results);
        errorMessage = (TextView) findViewById(R.id.error_message);

        queryField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    presenter.onSearchButtonClicked(v.getText().toString());
                }
                return true;
            }
        });

        queryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onSearchButtonClicked(queryField.getText().toString());
            }
        });

        adapter = new MLResultListAdapter();
        resultList.setLayoutManager(new LinearLayoutManager(this));
        resultList.setAdapter(adapter);

        presenter = new MainActivityPresenter(this);
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.onSaveInstanceState(outState);
    }

    @Override
    public void showProgress() {
        //TODO
    }

    @Override
    public void hideProgress() {
        //TODO
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void setResults(List<Result> results) {
        errorMessage.setVisibility(View.GONE);
        resultList.setVisibility(View.VISIBLE);

        adapter.refresh(results);
    }

    @Override
    public void setErrorMessage(String message) {
        errorMessage.setVisibility(View.VISIBLE);
        resultList.setVisibility(View.GONE);

        errorMessage.setText(message);
    }
}
