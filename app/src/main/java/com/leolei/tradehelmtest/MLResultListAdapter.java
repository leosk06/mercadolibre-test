package com.leolei.tradehelmtest;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by leolei on 2016/2/17.
 */

public class MLResultListAdapter extends RecyclerView.Adapter<MLResultListAdapter.ViewHolder> {

    private List<Result> data;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Result item = data.get(position);
        holder.name.setText(item.getTitle());
        holder.price.setText(item.getCurrency_id() + item.getPrice());

        if (position == 0) {
            holder.itemView.setBackgroundColor(Application.getInstance().getResources().getColor(R.color.green));
        } else if (position == data.size() - 1) {
            holder.itemView.setBackgroundColor(Application.getInstance().getResources().getColor(R.color.red));
        } else {
            holder.itemView.setBackgroundColor(Application.getInstance().getResources().getColor(android.R.color.white));
        }
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public void refresh(List<Result> results) {
        this.data = results;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public TextView price;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.product_name);
            price = (TextView) itemView.findViewById(R.id.product_price);
        }
    }
}
