package com.leolei.tradehelmtest;

import com.squareup.otto.Bus;

/**
 * Created by leolei on 2016/2/17.
 */

public class EventBus {
    private static Bus instance;

    private EventBus() {
    }

    public static Bus getInstance() {
        if (instance == null) {
            instance = new Bus();
        }
        return instance;
    }
}
