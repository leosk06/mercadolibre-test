package com.leolei.tradehelmtest;

import java.util.List;

/**
 * Created by leolei on 2016/2/17.
 */

// Class for Gson to parse JSON into. This does not belong to the model proper, it's just an auxiliary class
public class MLResult {
    private List<Result> results;

    public List<Result> getResults() {
        return results;
    }
}
